package com.pet;

import com.pet.dao.PictureDAO;
import com.pet.entity.dos.PictureDO;
import com.pet.entity.dto.PicQueryDTO;
import com.pet.entity.vo.PicTagVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class PetPictureBackendApplicationTests {

    @Autowired
    private PictureDAO pictureDAO;
    @Test
    public void testPictureDAO(){
        /** 测试条件查询
        PicQueryDTO pic = new PicQueryDTO();
        pic.setKeyword("猫");
        pic.setTag("宠物");
        pic.setStartIndex(1);
        pic.setDesc(false);
            List<PictureDO> picList = pictureDAO.selectPictureList(pic);
            System.out.println(picList.size());
            if(picList.size() > 0){
                for (PictureDO pictureDO : picList) {
                    System.out.println(pictureDO);
                }
            }
        **/
        List<PicTagVO> list = pictureDAO.selectTagPicList();
        list.stream().forEach(picTagVO -> System.out.println(picTagVO));
    }

}

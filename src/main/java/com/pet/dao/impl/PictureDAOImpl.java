package com.pet.dao.impl;
/*-----------------------------------
 *   @Author: uni
 *   @Time: 2024/3/18 21:05   
 *   @Description: 
-----------------------------------*/

import com.pet.dao.PictureDAO;
import com.pet.entity.dos.PictureDO;
import com.pet.entity.dto.PicQueryDTO;
import com.pet.entity.vo.PicTagVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PictureDAOImpl implements PictureDAO {

    @Autowired
    private PictureDAO pictureDAO;
    @Override
    public List<PictureDO> selectPictureList(PicQueryDTO pic) {
        return pictureDAO.selectPictureList(pic);
    }

    @Override
    public Integer countPictureList(PicQueryDTO pic) {
        return pictureDAO.countPictureList(pic);
    }

    @Override
    public List<PicTagVO> selectTagPicList() {
        return pictureDAO.selectTagPicList();
    }

    @Override
    public boolean insert(PictureDO pic) {
        return pictureDAO.insert(pic);
    }
}

package com.pet.service;
/*-----------------------------------
 *   @Author: uni
 *   @Time: 2024/3/18 21:03   
 *   @Description: 
-----------------------------------*/

import com.pet.entity.dos.PictureDO;
import com.pet.entity.dto.PicQueryDTO;
import com.pet.entity.vo.PicTagVO;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface PictureService {
    public List<PictureDO> queryPicture(PicQueryDTO pic);

    public Integer countPicture(PicQueryDTO pic);

    public List<PicTagVO> queryPicTag();

    public PictureDO upload(String detail, String tag, MultipartFile file) throws IOException;
}

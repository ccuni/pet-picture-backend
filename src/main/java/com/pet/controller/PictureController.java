package com.pet.controller;

import com.pet.entity.dos.PictureDO;
import com.pet.entity.dto.PicQueryDTO;
import com.pet.entity.vo.PicTagVO;
import com.pet.entity.vo.RespVO;
import com.pet.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
public class PictureController {

    @Autowired
    private PictureService pictureService;

    @GetMapping("/picture")
    public RespVO<List<PictureDO>> queryPicture(PicQueryDTO pic){
        return RespVO.success("查询成功", pictureService.queryPicture(pic));
    }

    @GetMapping("/picture/count")
    public RespVO<Integer> countPicture(PicQueryDTO pic){
        return RespVO.success("查询成功", pictureService.countPicture(pic));
    }

    @GetMapping("/picture/tag")
    public RespVO<List<PicTagVO>> queryPicTag(){
        return RespVO.success("查询成功", pictureService.queryPicTag());
    }


    @PutMapping("/picture")
    public RespVO<PictureDO> addPicture(
            String detail, String tag, MultipartFile file) throws IOException {
        return RespVO.success("添加成功", pictureService.upload(detail, tag, file));
    }


}

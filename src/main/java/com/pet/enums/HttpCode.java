package com.pet.enums;
/*-----------------------------------
 *   @Author: uni
 *   @Time: 2024/3/18 21:10   
 *   @Description: 
-----------------------------------*/

import lombok.AllArgsConstructor;
@AllArgsConstructor
public enum HttpCode {
    OK("200", "请求成功"),
    CREATED("201", "已创建"),
    BAD_REQUEST("400", "错误的请求"),
    UNAUTHORIZED("401", "未经授权"),
    NOT_FOUND("404", "未找到");

    private final String code;
    private final String text;

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}

package com.pet.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// DTO层: 图片搜索实体类
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PicQueryDTO {
    private String detail;         // 图片描述
    private String tag;             // 图片标签
    private boolean desc;           // 是否按时间降序, 默认升序（即按最早发布），
    private Integer startIndex;     // 从第几条记录开始
}

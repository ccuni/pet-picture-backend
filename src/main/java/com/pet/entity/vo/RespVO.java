package com.pet.entity.vo;

import com.pet.enums.HttpCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// VO层 后端响应实体类
@Data
@NoArgsConstructor
public class RespVO<T> {
    private String code;      // 响应代码: 遵循状态码枚举
    private String msg;       // 响应消息
    private T data;           // 响应数据
    public RespVO(String code, String msg, T data){
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    public RespVO(String code, String msg){
        this.code=code;
        this.msg=msg;
    }
    public static RespVO success(String msg){
        return new RespVO(HttpCode.OK.getText(), msg);
    }
    public static <T> RespVO success(String msg, T data){
        return new RespVO(HttpCode.OK.getCode(), msg, data);
    }
}

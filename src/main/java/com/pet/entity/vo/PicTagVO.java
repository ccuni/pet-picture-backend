package com.pet.entity.vo;

import com.pet.entity.dos.PictureDO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*-----------------------------------
 *   @Author: uni
 *   @Time: 2024/3/19 23:06   
 *   @Description: 
-----------------------------------*/
// VO层 图片标签实体类
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PicTagVO {
    private PictureDO pic;      // 当前标签下的最新图片
    private Integer total;      // 统计图片数量
}

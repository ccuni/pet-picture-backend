package com.pet.entity.dos;

import java.util.Date;
// DO层: 游客实体类
public class VisitorDO {
    private String ip;      // IP地址
    private Date firstTime; // 初次访问时间
    private Date lastTime;  // 最后访问时间
}

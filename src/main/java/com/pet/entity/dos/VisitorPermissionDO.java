package com.pet.entity.dos;
// DO层：游客权限实体类
public class VisitorPermissionDO {
    private String ip;
    private boolean canVisitHtml;
    private boolean canVisitPic;
}

package com.pet.entity.dos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

// DO层：图片实体类
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PictureDO {
    private String detail;      // 图片描述
    private Date createTime;    // 创建时间
    private String imgUrl;      // 图片地址
    private String tag;         // 图片标签
}
